import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Message } from './message';
import { MessageService } from '../message.service';
import { error } from 'util';
import { UserCircleService } from '../user-circle.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messages = [];
  userdata: string;
  message: Message;
  msgToSend: string;
  receiver: string;
  circle: string;
  type: string;
  newUser: string;
  @Input() messageObj: object;

  constructor(private messageService: MessageService, private usrCirService : UserCircleService) { }

  ngOnChanges(value) {

    this.type = value.messageObj.currentValue.type;
    this.receiver = value.messageObj.currentValue.value;
    
    if (this.type === 'user') {      
      this.messageService.getMessagesFromUser(this.receiver).subscribe(
        data => {
          this.messages = data.json();
        });

    } else {
      this.messageService.getMessagesByCircle(this.receiver).subscribe(
        data => {
          this.messages = data.json();
        });
    }

  }

  onClick(message: any, type: any) {

    this.message = new Message();
    this.message.message = message;
    this.message.senderName = localStorage.getItem('username');

    if (type === 'user') {
    this.message.receiverId = this.receiver;
      this.messageService.sendMessageToUser(this.message).subscribe(
        data => {
          if (data.status === 200) {
            let obj = {
              'senderName': localStorage.getItem('username'),
              'receiverId': this.receiver,
              'message': message
            }
            this.messages.push(obj);
            this.msgToSend = '';
          }
        },
      error => {
        alert('Something went wrong, please try after sometime');
      });
    } else {
      this.message.circleName = this.receiver;
      this.messageService.sendMessageToCircle(this.message).subscribe(
        data => {
          if (data.status === 200) {
            let obj = {
              'senderName': localStorage.getItem('username'),
              'circleName': this.receiver,
              'message': message
            }
            this.messages.push(obj);
            this.msgToSend = '';
          }
        },
        error => {
          alert('Something went wrong, please try after sometime');
        });
    }
  }

  addUserToCircle(user: string){
    this.usrCirService.addUserToCircle(user,this.receiver).subscribe(
      data=>{
        if(data.status === 200){          
          alert('\"'+user+'\" user successfully added to '+this.receiver+' cirlce');
          this.newUser = '';
        } else  {
          alert('Something went wrong, Please try after sometime');
        }
      },
      error => {
          alert('Something went wrong, please try after sometime'); 
      }
    )
  }
  ngOnInit() {

  }
}
